package sematec.java.lib.pojo;

public class StudentModel extends HumanModel {
    private String studentCode;
    private String gender ;
    int level ;

   public StudentModel(String stdCode){
       this.studentCode = stdCode ;
   }

    public StudentModel() {
    }

    public StudentModel(String studentCode, String gender, int level) {
        this.studentCode = studentCode;
        this.gender = gender;
        this.level = level;
    }

    public String getStudentCode() {
        return studentCode  ;
    }

    public void setStudentCode(String studentCode) {
        this.studentCode = studentCode;
    }
}
